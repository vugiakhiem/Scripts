local startTime = os.clock()

local KeyGuardLibrary = loadstring(game:HttpGet("https://cdn.keyguardian.org/library/v1.0.0.lua"))()
local trueData = "6acc399280394554a52fccc7be6e16de"
local falseData = "0678746ca81a47e394e91cd225405a3c"
local publicToken = "71c5227731e34adb9eed387f2aebeeb4"
local privateToken = "986328ba5cb54a8a83ed6023b00e0c1d"

if not (publicToken and publicToken ~= "" and privateToken and privateToken ~= "" and trueData and trueData ~= "" and falseData and falseData ~= "") then
    warn("[KeyGuard] - Missing required information for initialization. Please check the tokens and data.")
    return
end

KeyGuardLibrary.Set({
    publicToken = publicToken,
    privateToken = privateToken,
    trueData = trueData,
    falseData = falseData,
})

local Directory = "KeyGuard.txt"

function ValidateSaved()
    local storedKey = readfile(Directory)
    if storedKey and storedKey ~= "" then
        local verificationStartTime = os.clock()

        local response = KeyGuardLibrary.validateDefaultKey(storedKey)
        local verificationEndTime = os.clock()
        local verificationTime = verificationEndTime - verificationStartTime
        print("Time taken to verify stored key: " .. verificationTime .. " seconds")
        if response == trueData then
            print("Saved Key is valid")
            return true
            else
            print("Saved Key is invalid")
            delfile(Directory)
            return false
        end
    end
end

if isfile(Directory) then
    if ValidateSaved() then
        return
    end
end

local Fluent = loadstring(game:HttpGet("https://github.com/dawid-scripts/Fluent/releases/latest/download/main.lua"))()

local Window = Fluent:CreateWindow({
    Title = "Key System",
    SubTitle = "",
    TabWidth = 160,
    Size = UDim2.fromOffset(520, 320),
    Acrylic = false,
    Theme = "Dark",
    MinimizeKey = Enum.KeyCode.LeftControl
})

local Tabs = {
    KeySys = Window:AddTab({ Title = "Key System", Icon = "key" }),
}

local InputKey = Tabs.KeySys:AddInput("InputKey", {
    Title = "Input Key",
    Description = "Insert your key...",
    Default = "",
    Placeholder = "Enter key…",
    Numeric = false,
    Finished = false
})

local Checkkey = Tabs.KeySys:AddButton({
    Title = "Check Key",
    Description = "Enter Key before pressing this button",
    Callback = function()
        print(InputKey.Value)
        local response = KeyGuardLibrary.validateDefaultKey(InputKey.Value)
	print(InputKey.Value)
        if response == trueData then
            print("Key is valid")
            writefile(Directory, InputKey.Value)
            Window:Destroy()
        else
            print("Key is invalid")
        end
    end
})

local Getkey = Tabs.KeySys:AddButton({
    Title = "Get Key",
    Description = "Get Key here",
    Callback = function()
        setclipboard(KeyGuardLibrary.getLink())
    end
})

Window:SelectTab(1)

local endTime = os.clock()
local loadTime = endTime - startTime
print("Time taken to load everything: " .. loadTime .. " seconds")